% ages=[52,53,48,34,62] mean:49.80 std: 10.21
% years of xp yoe=[24,25,20,4,25] mean:19.60 std: 8.96
function runAll()
    [meanScore1, score1, methodScorePerQuestion1] = analyzeResults('1001-ratings.txt');
    [meanScore2, score2, methodScorePerQuestion2] = analyzeResults('1002-ratings.txt');
    [meanScore3, score3, methodScorePerQuestion3] = analyzeResults('1003-ratings.txt');
    [meanScore4, score4, methodScorePerQuestion4] = analyzeResults('1004-ratings.txt');
    [meanScore5, score5, methodScorePerQuestion5] = analyzeResults('1005-ratings.txt');
    
    
    %Columns are the scores, rows are the methods, values are frequency
    score = score1 + score2 + score3 + score4 + score5;     
    for mIdx=1:7
       %Flip to map choice numbers to selection
       a = fliplr(score(mIdx,:));    
       figure; histogram('BinEdges', -2.5:2.5, 'BinCounts', a)   
       xlabel('Score')
       ylabel('Bin Count')    
       xlim([-2.5,2.5])
       set(gca,'xtick',-2:2)
    end
    
    meanScores = (meanScore1+ meanScore2 + meanScore3 + meanScore4 + meanScore5)/5
    methodScorePerQuestion = methodScorePerQuestion1 + methodScorePerQuestion2 + methodScorePerQuestion3 + methodScorePerQuestion4+ methodScorePerQuestion5
end
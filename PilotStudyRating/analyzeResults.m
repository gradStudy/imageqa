% Replace double questions , to ;
% i.e. "choice":{"1":5,"2":5}} to "choice":{"1":5;"2":5}}
% Place m,q,c row at top
% analyzeResults('1001-Ratings.txt')
% methodScorePerQuestion: [N_SCREENS] x [2xN_METHODS] scores matrix for
% double questions [10x14]
function [meanScore, score, methodScorePerQuestion] = analyzeResults(fileName)
    data=readtable(fileName,'Delimiter',[','],'TextType','string');
    
    N_SCREENS = 10;
    N_METHODS = 7;
    N_CASES = N_SCREENS*N_METHODS;
    
    %Extract the method
    method = data.m;
    %Find this number easily by: regexp(method,'\d')
    mNums = extractAfter(method,10);
    question = data.q;
    
    %Extract the method number: mN
    %Extract the SCREEN number: qN
    qNums = extractAfter(question,9);
    for i=1:N_CASES
       mN(i)=str2num(mNums{i});
       qN(i)=str2num(qNums{i});
    end
            
    %Create score table for methods
    %Every row is a method
    score = zeros(7,5); 
    methodScorePerQuestion = zeros(N_SCREENS,2*N_METHODS);
    
    %Now extract the choices and fill the score table
    %Be careful - not to mix the choice to sub-question number!!!
    choice = data.c;
    cNums = extractAfter(choice,9);
    %Get length
    for i=1:N_CASES
        %Get score of each question
        %Double choice for question 1-6-9
        if (length(char(cNums(i)))>6) %or check question number qN
            str = split(choice(i),';');
            str1=erase(str(1),'choice:{"1":');
            c1=str2num(str1{1});
            score(mN(i),c1)=score(mN(i),c1)+1;            
            methodScorePerQuestion(qN(i), mN(i)) = convertToRank(c1);
            
            str2=erase(str(2),'"2":');
            str2=erase(str2,'}}');
            c2=str2num(str2{1});
            score(mN(i),c2)=score(mN(i),c2)+1;       
            methodScorePerQuestion(qN(i), mN(i)+7) = convertToRank(c2);
        else
            str=erase(choice(i),'choice:{"1":');
            str=erase(str,'}}');
            c1=str2num(str{1});
            score(mN(i),c1)=score(mN(i),c1)+1;
            methodScorePerQuestion(qN(i), mN(i)) = convertToRank(c1);
        end
    end
       
    
    %For every method - find score
    for mIdx=1:7
       %Flip to map choice numbers to selection
       a = fliplr(score(mIdx,:));    
       %figure; histogram('BinEdges', -2.5:2.5, 'BinCounts', a)
       b=-2:2;
       meanScore(mIdx) = mean(a.*b);
    end
end

function out=convertToRank(val)
    switch val
        case {1}
            out=2;
        case {2}
            out=1;
        case {3}
            out=0;
        case {4}
            out=-1;
        otherwise
            out=-2;
    end
end

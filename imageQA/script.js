var {ipcRenderer, remote} = require('electron');
var {fork} = require('child_process');
var process = require('process');
var path = require('path');

var scrollBox = document.getElementById('scrollBox');
var softSubmit = document.getElementById('submit-button');
var softStop = document.getElementById('stop-button');
var form = document.getElementById('myForm');

var child;

function processForm(e) {
    if (e.preventDefault) e.preventDefault();

    softSubmit.disabled = true;
    softStop.disabled = false;

    //Start server.js if 0 with n clients - Else start related experiment number
    child = fork(__dirname+path.sep+'app.js',
      [
        `--e=${form.elements[0].value}`
      ],
      {
        cwd: __dirname
      })

    ipcRenderer.send('async', child.pid)

    // You must return false to prevent the default form behavior
    return false;
}

if (form.attachEvent) {
    form.attachEvent("submit", processForm);
} else {
    form.addEventListener("submit", processForm);
}

softStop.onclick = function(){
    softSubmit.disabled = false;
    softStop.disabled = true;
    child.kill();
    console.log(`Killed child ${child.pid}`);
    scrollBox.innerHTML += `Killed child ${child.pid}\n`
    scrollBox.scrollTop = scrollBox.scrollHeight;
}

ipcRenderer.on('async-reply', (event, arg) => {
    console.log(arg);
    scrollBox.innerHTML += `${arg}\n`
    scrollBox.scrollTop = scrollBox.scrollHeight;
});

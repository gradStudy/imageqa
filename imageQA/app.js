var express = require('express')
var bodyParser = require('body-parser');
var path = require('path');
var fs = require('fs')
var _ = require('underscore')


//Get page requests
var app = express()
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//set the client content folder
app.use(express.static(path.join(__dirname, 'public')));

var args = require('yargs')
  .options({
    e: {
      demand: true,
      describe: 'Experiment No',
      alias: 'expNo'
    }
  })
  .argv

app.get('/', (req, res) => {
  res.sendFile('./index.html', {root:__dirname})
})


/* Listens on port 8000.
*/
app.listen(8000, function(){
		console.log('listening on port 8000')
})


var WebSocketServer = require('ws').Server
var wss = new WebSocketServer({port : 9999})


let N_REFIMAGES = 10  //This is equal to number of cases
let N_METHODS = 7    //This is equal to number of methods

let N_IMAGES = N_REFIMAGES*N_METHODS       //Image and method number is always the same
let testStarted = false
let finishTest = false
//let qArr = shuffle(_.range(N_IMAGES)) //For shuffling the questions
//Make question random sequence fixed for each trial
let qArr = [ 20,  64,  0,  49,  4,  9,  60,  31,  18,  63,  40,  16,  27,  61,  5,  50,  38,  28,  46,  55,  32,  22,  15,  7,  57,  11,  24,  53,  44,  14,  43,  13,  29,  56,
   3,  65,  21,  45,  2,  68,  48,  67,  23,  35,  42,  51,  26,  58,  39,  10,  41,  30,  69,  36,  54,  62,  12,  37,  6,  66,  47,  52,  25,  59,  19,  33,  1,  17,  34,  8 ]
//console.log(qArr)
let qNo=0  //Counter for screens and cases

let L_CASE = 0 //Global variable for case label
let N_QUESTIONS = 1
let qSNo = 0 //Global variable for array index of case number

let N_METHOD = 0

//Define questions
//Saves number of sub questions per page
qs=[2,1,1,1,1,2,1,1,2,1]

//Saves the sub questions for each question number indexed
let qStrs = {}
qStrs[0]=" - Granüler kabalaşma ne ölçüde belirginleşmiştir? \n - Nodül kontur keskinliği ne ölçüde belirginleşmiştir?"
qStrs[1]=" - Tiroid konturu görüntü kalitesi ne ölçüde artmıştır?"
qStrs[2]=" - Granüler kabalaşma ne ölçüde belirginleşmiştir?"
qStrs[3]=" - Kist ne ölçüde belirginleşmiştir?"
qStrs[4]=" - Mikrokalsifikasyonların gözlemlenebilirliği ne kadar artmıştır?"
qStrs[5]=" - Cilt altı yağ dokusu keskinliği ne ölçüde iyileşmiştir? \n - Nodül kontur keskinliği ne ölçüde belirginleşmiştir?"
qStrs[6]=" - Submandibular bezdeki heterojenite gözlemlenebilirliği ne kadar artmıştır?"
qStrs[7]=" - Parotis bezindeki heterojenite gözlemlenebilirliği ne kadar artmıştır?"
qStrs[8]=" - Tendon çevre doku geçişi ne ölçüde belirginleşmiştir? \n - Tendon homojenitesi ne ölçüde artmıştır? "
qStrs[9]=" - Granüler kabalaşma ne ölçüde belirginleşmiştir?"

let qExps = {}
qExps[0]=" * Görüntü meme fantomundan alınmıştır \n"
qExps[1]=""
qExps[2]=""
qExps[3]=" * Görüntü meme fantomundan alınmıştır  \n"
qExps[4]=""
qExps[5]=""
qExps[6]=""
qExps[7]=""
qExps[8]=""
qExps[9]=""

//Split sub questions
//let k= qStrs[1].split("\n");
//console.log("Qs: ", k[1])

//Check and put warning if necessary
if (qs.length != N_REFIMAGES)
  console.log("Warning: Number of questions is not consistent with definitions!")


wss.on('connection', function connection(ws) {

  ws.send(JSON.stringify({type: "startTest"}))

  /* Whenever a message comes from the client */
  ws.on('message', function incoming(message) {
    message = JSON.parse(message)
    switch(message.type) {
      case 'keyEvent':
        if (true==testStarted){
            var keypressMessage = {}
            /* Assign client id to keypressMessage */
            keypressMessage.method = N_METHOD
            keypressMessage.question = L_CASE
            keypressMessage.choice = message.choiceArray

            /* Writes the whole keypressMessage to the choiceData.txt file */
            //fs.writeFile(`${args.expNo}-ratings.txt`, JSON.stringify(keypressMessage) + '\n', {flag:'a'}, function(err){

            //Record method and decision
            //TODO: In final version toggle below comment!!!!
            //fs.writeFile(`${args.expNo}-ratings.txt`, "Method: ", method, "Question: ", qSNo1, JSON.stringify(keypressMessage), '\n', function(err){
              fs.writeFile(`${args.e}-ratings.txt`, JSON.stringify(keypressMessage) + '\n', {flag:'a'}, function(err){
               if(err) throw err
          })
        }

        if (false == finishTest){
          qSNo = qArr[qNo] % N_METHODS //Repeat questions for methods
          N_METHOD = qSNo+1
          L_CASE =  parseInt(qArr[qNo] / N_METHODS) + 1 //Get question number
          N_QUESTIONS = qs[L_CASE-1]   //Repeat the number of subquestions for methods

          var fNameRef = "url('../images/" + "ref" + L_CASE + ".png')"
          var fNameComp = "url('../images/" + "comp" + L_CASE + '_' + N_METHOD + ".png')"

          console.log("Q no: ", qArr[qNo], "L_CASE: ", L_CASE)

          ws.send(JSON.stringify({type: "displayNewImage", ref: fNameRef, comp: fNameComp, nQs: N_QUESTIONS, questionString: qStrs[L_CASE-1], questionExplanation: qExps[L_CASE-1] }))
          testStarted = true //Start the test by sending the first images for evaluation
          qNo++
          if (qNo==N_IMAGES)
            finishTest = true
        }
        else{
          ws.send(JSON.stringify({type: "finishTest", ref: fNameRef, comp: fNameComp}))
        }

        break;
      }
  })
})

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
}

let SCREEN_WELCOME = 0
let SCREEN_TEST = 1
let SCREEN_FINISH = 2

let currentScreen = SCREEN_WELCOME

let welcomeScreen = document.getElementsByClassName('welcomeScreen')[0]
let testScreen = document.getElementsByClassName('testScreen')[0]
let finishScreen = document.getElementsByClassName('finishScreen')[0]


let ref  //Images to be displayed on current screen
let comp

//The variables for experiment
let N_QUESTIONS = 1  //Number of questions to be displayed to participant
let N_CHOICES = 5  //Choices for participant

//Radio button labels
let radButtonValueString = {}
radButtonValueString[0] = "Çok Daha İyi (+2)"
radButtonValueString[1] = "İyi (+1)"
radButtonValueString[2] = "Aynı (0)"
radButtonValueString[3] = "Kötü (-1)"
radButtonValueString[4] = "Çok Kötü (-2)"

ref = document.getElementsByClassName('refImage')[0]
comp = document.getElementsByClassName('compImage')[0]


/*
* Opens a websocket on port 9999
*/
var wsc = new WebSocket("ws://" + window.location.hostname + ":9999/")
console.log('wsc: ', window.location.hostname)


document.addEventListener('keydown', event => {
  ////console.log("Keydown")
  if (event.keyCode === 32) {  //get any key from http://keycode.info/
    if (SCREEN_TEST==currentScreen){
      let chosen = {}

      let elementStr
      let i

      let chosenFlag

      for (j=1; j<=N_QUESTIONS;j++){
        elementStr = "r"
        //console.log("el str: ", elementStr)
        chosenFlag = false
        for (i=1; i<=N_CHOICES; i++){
          elementStr = "r"
          elementStr += j    //i.e. r1_1  r1_2 or r_3_5
          elementStr += "_"
          elementStr += i
          if (document.getElementById(elementStr).checked){
            console.log("Chosen element: ", elementStr)
            chosen[j] = i
            chosenFlag = true
          }
        }
        if (false == chosenFlag){
          console.log("Nothing selected!")
         // j=N_QUESTIONS
          break
         // $( ".warning" ).html('UYARI: Lütfen seçim yapınız!');
        }
      }

        //TODO: Uncheck after each load
        //insert all Number of questions and radio buttons
        //Questions string

      console.log("Chosen ones: ", chosen)
      //TODO: Null check
      if (false == chosenFlag){
        console.log("Uyarı")
        $( ".warning" ).html('UYARI: Lütfen seçim yapınız!');
      }
      else
      {
        wsc.send(JSON.stringify({type: 'keyEvent', keyPressed: 'space', choiceArray: chosen}))
        //Clear choices
        $( ".jsItemsContainer" ).html('')
        $( ".warning" ).html('')
      }
    }
    else
      wsc.send(JSON.stringify({type: 'keyEvent', keyPressed: 'space'}))
  //  //console.log("Pressed space.")
  }
})

/* Doesn't matter to which number it is set, because it is being set by the server */
var trialNo = 1

/*
* Whenever a connection is opened, sends the screen size to the server as setScreenSize
*/
wsc.onopen = function() {
  var w = Math.min(document.documentElement.clientWidth, window.innerWidth || 0)
  var h = Math.min(document.documentElement.clientHeight, window.innerHeight || 0)
  wsc.send(JSON.stringify({type: 'setScreenSize', width: w, height: h}))
}

/*
* Handles messages coming from the server
*/
wsc.onmessage = function(event){
   var message = JSON.parse(event.data)
  // //console.log("Message from server: " + message.type)
   switch (message.type) {
    case "startTest":  //load all images and show default
      loadStartScreen()
      break;
     case "displayNewImage":  //load all images and show default
       currentScreen = SCREEN_TEST
       var refimageName = message.ref
       var compimageName = message.comp
       N_QUESTIONS = message.nQs

       let QStr = message.questionString
       let qArr = QStr.split("\n")

       console.log("im1: ", refimageName, " im2: ", compimageName)
       loadImages(refimageName,compimageName)

       let elementStr

       $( ".warning2" ).html(message.questionExplanation);

       for (j=1; j<=N_QUESTIONS;j++){
        let q = "<br>" + qArr[j-1] +  "<br><br>"
        $( ".jsItemsContainer" ).append(q)
        let qRad = ""  //Radio button dom element creation string
        for (i=1; i<=N_CHOICES; i++){
          elementStr = "r"
          elementStr += j    //i.e. r1_1  r1_2 or r_3_5
          let nameStr = elementStr
          elementStr += "_"
          elementStr += i
          //console.log("insert element: ", elementStr)
          qRad += "<label class='container'>" + radButtonValueString[i-1] + "&nbsp; &nbsp; &nbsp; <input type='radio' id='" + elementStr + "' name='" + nameStr + "'>  <span class='checkmark'></span>   </label>"
          $( ".jsItemsContainer" ).append(qRad)
          qRad = ""
        }}


       break;
    case "finishTest":  //load all images and show default
      loadFinishScreen()
      break;
  }
 }


 function loadImages(refIm,compIm){
  ref.style.backgroundImage = refIm
  ref.style.backgroundRepeat = "no-repeat"

  comp.style.backgroundImage = compIm
  comp.style.backgroundRepeat = "no-repeat"


  ref.style.display = "block"
  comp.style.display = "block"
  testScreen.style.display = "block"
  welcomeScreen.style.display = "none"
  finishScreen.style.display = "none"
}


function loadStartScreen(){
  welcomeScreen.style.display = "block"
  testScreen.style.display = "none"
  finishScreen.style.display = "none"
}

function loadFinishScreen(){
  welcomeScreen.style.display = "none"
  testScreen.style.display = "none"
  finishScreen.style.display = "block"
}

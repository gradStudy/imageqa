var process = require('process');
var path = require('path')
const electron = require('electron')
const {app, BrowserWindow, ipcMain} = electron

var childPID;
var ipcEvent;
ipcMain.on('async', (event, arg) => {
   ipcEvent = event;
   childPID = arg
   console.log(`-Main: Renderer forked child ${childPID}`);
   event.sender.send('async-reply', `-Main: Renderer forked child ${childPID}`);
});
var mainWindow = null;
app.on('ready', function() {
  // Create the browser window.
  mainWindow = new BrowserWindow({width: 600, height: 580, icon: path.join(__dirname, 'assets/imageQAicon.png')});
  mainWindow.loadURL('file://' + __dirname + '/index.html');
});
